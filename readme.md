## School Data Management

This Java program is designed for managing student and teacher information in a school. It uses Generics to ensure code flexibility and Lambdas to sort and filter records.

### Features

This program allows you to:

1. **Add Students and Teachers:** You can add students and teachers and store their information.

2. **Search for Students and Teachers by Name:** The program helps you find students and teachers by their name.

3. **Sort Students and Teachers:** Students are sorted by their birthdate, while teachers are sorted by their hire date.

4. **Filter Students and Teachers:** You can filter students and teachers based on various criteria, such as age for students or subject for teachers.

### Usage

1. Add students and teachers by using the `addStudent` and `addTeacher` methods.

2. Search for students or teachers by name using the `searchStudentByName` and `searchTeacherByName` methods.

3. Sort students by birthdate using the `sortStudentsByBirthDate` method, and sort teachers by hire date using the `sortTeachersByHireDate` method.

4. Filter students by age using the `filterStudentsByAge` method, or filter teachers by subject using the `filterTeachersBySubject` method.

Feel free to use this program to manage school data efficiently. If you have any questions or need assistance, please don't hesitate to reach out.
 ```plantuml
!include https://raw.githubusercontent.com/patrik-csak/one-dark-plantuml-theme/v1.0.1/theme.puml

class Person {
    - name: String
    - birthDate: LocalDate
    + Person(name: String, birthDate: LocalDate)
    + getName(): String
    + getBirthDate(): LocalDate
}

class Student {
    + Student(name: String, birthDate: LocalDate)
}

class Teacher {
    - subject: String
    - hireDate: LocalDate
    + Teacher(name: String, birthDate: LocalDate, subject: String, hireDate: LocalDate)
    + getSubject(): String
    + getHireDate(): LocalDate
}

class SchoolManagement {
    - students: List<Student>
    - teachers: List<Teacher>
    + addStudent(student: Student)
    + addTeacher(teacher: Teacher)
    + searchStudentByName(name: String): List<Student>
    + searchTeacherByName(name: String): List<Teacher>
    + sortStudentsByBirthDate(): List<Student>
    + sortTeachersByHireDate(): List<Teacher>
    + filterStudentsByAge(age: int): List<Student>
    + filterTeachersBySubject(subject: String): List<Teacher>
}

Person --|> Student
Person --|> Teacher
Student --|> SchoolManagement
Teacher --|> SchoolManagement


   ```